package com.bontouch.kuf.themesandstyles;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

public class MyCustomComponent extends View {
    private Paint mPrimaryPaint;
    private Paint mSecondaryPaint;
    private float mCirclePercentage;
    private int mPrimaryColor;
    private int mSecondaryColor;
    private float mStrokeWidth;

    public MyCustomComponent(Context context) {
        super(context);
        mPrimaryColor = Color.RED;
        mSecondaryColor = Color.BLUE;
        mStrokeWidth = 5;
        mCirclePercentage = 0.9f;
        setupPaint();
    }

    public MyCustomComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        readAttributes(context, attrs);
        setupPaint();
    }

    public MyCustomComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        readAttributes(context, attrs);
        setupPaint();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MyCustomComponent(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        readAttributes(context, attrs);
        setupPaint();
    }

    private void readAttributes(Context context, AttributeSet attrs) {
        TypedArray a = null;
        try {
            a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.CustomComponent,
                    0, 0);
            mPrimaryColor = a.getColor(R.styleable.CustomComponent_primaryColor, Color.RED);
            mSecondaryColor = a.getColor(R.styleable.CustomComponent_secondaryColor, Color.BLUE);
            mStrokeWidth = a.getFloat(R.styleable.CustomComponent_strokeWidth, 5);
            mCirclePercentage = a.getFloat(R.styleable.CustomComponent_percentage, 0.9f);
        } finally {
            if (a != null) {
                a.recycle();
            }
        }
    }

    private void setupPaint() {
        mPrimaryPaint = new Paint();
        mPrimaryPaint.setColor(mPrimaryColor);
        mPrimaryPaint.setStyle(Paint.Style.FILL);
        mPrimaryPaint.setAntiAlias(true);
        mSecondaryPaint = new Paint();
        mSecondaryPaint.setColor(mSecondaryColor);
        mSecondaryPaint.setStrokeWidth(mStrokeWidth);
        mSecondaryPaint.setStyle(Paint.Style.STROKE);
        mSecondaryPaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float width = getWidth();
        float height = getHeight();
        float radius = ((width > height ? height : width) * 0.5f) * mCirclePercentage;
        canvas.drawCircle(width / 2, height / 2, radius, mPrimaryPaint);
        canvas.drawCircle(width / 2, height / 2, radius, mSecondaryPaint);
    }
}
