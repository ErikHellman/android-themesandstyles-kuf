# Styles and Themes for Android
* Styles are collections of properties (attributes and values)
* Styles are applied to Views
* Themes are Styles applied to an Application or Activity (and all Views in those Activities)
	* In Lollipop, you can put Themes on a View and it propagates to all children 
* A Theme can inherit from one other Theme (but not multiple)
	* Using parent=“OtherTheme”
	* Using dot-notation: MyTheme, MyTheme.Dark, MyTheme.Dark.LightActionBar
* Theme that should use Material Design and support pre-L should inherit Theme.AppCompat
* R.style contains all your defined styles
	* android.R.style contains all built-in platform styles
	* android.support.v7.appcompat.R.style are the support library styles (there is also one for card view)
* R.stylable contains all attributes that can be styled 
* Attributes from platform must be prefixed with android: in a theme
* Attributes from libraries (including support lib) have no prefix
	* Beware of name collisions!
* Custom attributes have two use cases
	* Attributes for custom Views
	* Reusable references
* Attributes have a name and a format
* Valid formats are
	* boolean
	* color
	* dimension
	* enum 
	* flag (e.g., x|y|z)
	* float
	* fraction
	* integer
	* reference
	* string


